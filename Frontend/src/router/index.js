import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '@/views/Login.vue'
import WebShop from '@/views/WebShop.vue'
import Factory from '../views/Factory.vue'
import SignUp from '../views/SignUp.vue'
import Profile from '../views/ProfileView.vue'
import ShoppingView from '@/views/ShoppingView.vue'
import ManagerShoppingView from '@/views/ManagerShoppingView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: SignUp
    },
    {
      path: '/webshop',
      name: 'webshop',
      component: WebShop
    },
    {
      path: '/factory/:factoryId',
      name: 'factory',
      component: Factory
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/shopping/:userId',
      name: 'shopping',
      component : ShoppingView
    },
    {
      path: '/managerShopping/:factoryId',
      name: 'managerShopping',
      component : ManagerShoppingView
    },
    
  ]
})

export default router
