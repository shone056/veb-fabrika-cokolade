package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import beans.Chocolate;
import beans.Chocolate.StockStatus;
import beans.Comment;
import beans.Product;


public class CommentDAO {
	
	private HashMap<String, Comment> comments = new HashMap<String, Comment>();
	private String contextPath;
	
	public CommentDAO() {
		
	}
	
	/***
	 * @param contextPath Putanja do aplikacije u Tomcatu. Moze se pristupiti samo iz servleta.
	 */
	public CommentDAO(String contextPath) {
		this.contextPath = contextPath;
		loadComments();
	}

	/***
	 * Vraca sve proizvode.
	 * @return
	 */
	public Collection<Comment> findAll() {
		return comments.values();
	}

	public Collection<Comment> findByFactoryId(String factoryId){
		System.out.println("Radim ovo");
		return comments.values()
				.stream()
				.filter(x -> x.getFactoryId().contains(factoryId))
				.collect(Collectors.toList());
	}
	
	public Comment deleteComment(String id) {
		Comment comment = comments.remove(id);
		saveComments();
		return comment;
	}
	
	/***
	 *  Vraca proizvod na osnovu njegovog id-a. 
	 *  @return Proizvod sa id-em ako postoji, u suprotnom null
	 */
	public Comment findComment(String id) {
		return comments.containsKey(id) ? comments.get(id) : null;
	}
	
//	public Chocolate updateChocolate(String id, Chocolate chocolate) {
//		Chocolate c = chocolates.containsKey(id) ? chocolates.get(id) : null;
//		if (c == null) {
//			return save(chocolate);
//		} else {
//			c.setName(chocolate.getName());
//			c.setPrice(chocolate.getPrice());
//			c.setType(chocolate.getType());
//			c.setWeight(chocolate.getWeight());
//			c.setDescription(chocolate.getDescription());
//			c.setStockStatus(chocolate.getStockStatus());
//			c.setStockQuantity(chocolate.getStockQuantity());
//		}
//		saveChocolates();
//		return c;
//	}
//	

	public Comment save(Comment comment) {
		Integer maxId = -1;
		for (String id : comments.keySet()) {
			int idNum =Integer.parseInt(id);
			if (idNum > maxId) {
				maxId = idNum;
			}
		}
		maxId++;
		comment.setId(maxId.toString());
		comments.put(comment.getId(), comment);
		saveComments();
		return comment;
	}

	/**
	 * Ucitava korisnike iz WebContent/users.txt fajla i dodaje ih u mapu {@link #products}.
	 * Kljuc je id proizovda.
	 * @param contextPath Putanja do aplikacije u Tomcatu
	 */

	private void loadComments() {
	    BufferedReader in = null;
	    try {
	        File file = new File(contextPath + "/comments.txt");
	        System.out.println(file.getCanonicalPath());
	        in = new BufferedReader(new FileReader(file));
	        String line;
	        while ((line = in.readLine()) != null) {
	            line = line.trim();
	            if (line.equals("") || line.indexOf('#') == 0)
	                continue;
	            String[] parts = line.split(";");
	            String id = parts[0].trim();
	            String buyerId = parts[1].trim();
	            String factoryId = parts[2].trim();
	            String content = parts[3].trim();
	            int rating = Integer.parseInt(parts[4].trim());

	            comments.put(id, new Comment(id, buyerId, factoryId, content, rating));
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (in != null) {
	            try {
	                in.close();
	            } catch (Exception e) { }
	        }
	    }
	}

	
	private void saveComments() {
	    System.out.println("ULAZIM OVDE");
	    BufferedWriter out = null;
	    try {
	        File file = new File(contextPath + "/comments.txt");
	        System.out.println("Fajl putanja: " + file.getCanonicalPath());
	        out = new BufferedWriter(new FileWriter(file, false)); // false to overwrite the file
	        for (Comment review : comments.values()) {
	            String line = String.join(";",
	                review.getId(),
	                review.getBuyerId(),
	                review.getFactoryId(),
	                review.getContent(),
	                String.valueOf(review.getRating())
	            );
	            out.write(line);
	            out.newLine();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (out != null) {
	            try {
	                out.close();
	            } catch (Exception e) { }
	        }
	    }
	}



}
