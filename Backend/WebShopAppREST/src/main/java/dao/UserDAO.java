package dao;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;

import beans.User;
import beans.User.Gender;
import beans.User.UserRole;
/***
 * <p>Klasa namenjena da ucita korisnike iz fajla i pruza operacije nad njima (poput pretrage).
 * Korisnici se nalaze u fajlu WebContent/users.txt u obliku: <br>
 * firstName;lastName;email;username;password</p>
 * <p><b>NAPOMENA:</b> Lozinke se u praksi <b>nikada</b> ne snimaju u istom tekstualnom obliku.</p>
 * @author Lazar
 *
 */
public class UserDAO {
	
	private HashMap<String, User> users = new HashMap<String, User>();
	private String contextPath;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	
	public UserDAO() {
		
	}
	
	/***
	 * @param contextPath Putanja do aplikacije u Tomcatu. Moze se pristupiti samo iz servleta.
	 */
	public UserDAO(String contextPath) {
		this.contextPath = contextPath;
		loadUsers();
	}
	
	/**
	 * Vraca korisnika za prosledjeno korisnicko ime i sifru. Vraca null ako korisnik ne postoji
	 * @param username
	 * @param password
	 * @return
	 */
	
	public User save(User user) {
		Integer maxId = -1;
		for (String id : users.keySet()) {
			int idNum =Integer.parseInt(id);
			if (idNum > maxId) {
				maxId = idNum;
			}
		}
		maxId++;
		user.setId(maxId.toString());
		users.put(user.getId(), user);
		saveUsers();
		return user;
	}
	
	public User find(String username, String password) {
		User user = null;
		boolean usernameExists = false;
		 for (User u : users.values()) {
	            if (u.getUsername().equals(username)) {
	                usernameExists = true;
	        		user = u;
	            }
	        }
		 if(usernameExists) {
			 if (user.getPassword().equals(password)) {
				 return user;
			 }
		 }
		 
		 return null;
	}
	
	public Collection<User> findAll() {
		return users.values();
	}
	
    public Collection<User> searchUser(String firstName, String lastName, String username) {
        return users.values().stream()
                .filter(user -> (firstName == null || user.getFirstName().toLowerCase().contains(firstName.toLowerCase())) &&
                                (lastName == null || user.getLastName().toLowerCase().contains(lastName.toLowerCase())) &&
                                (username == null || user.getUsername().toLowerCase().contains(username.toLowerCase())))
                .collect(Collectors.toList());
    }
	
	public Collection<User> findAvailableManagers(){
		Collection<User> availableManagers = new ArrayList<>();
		 for (User u : users.values()) {
	            if (u.getUserRole() == UserRole.MANAGER && u.getManagerFactoryId().equals("0")) {
	            	availableManagers.add(u);
	            }
	        }
		 
		 return availableManagers;
	}
	
	public Collection<User> findAllWorkers(){
		Collection<User> workers = new ArrayList<>();
		 for (User u : users.values()) {
	            if (u.getUserRole() == UserRole.WORKER) {
	            	workers.add(u);
	            }
	        }
		 
		 return workers;
	}
	public User findUser(String id) {
		return users.containsKey(id) ? users.get(id) : null;
	}
	
	public User updateUser(String id, User user) {
		User u = users.containsKey(id) ? users.get(id) : null;
		if(u == null) {
			return save(user);
		}else {
			u.setFirstName(user.getFirstName());
			u.setLastName(user.getLastName());
			u.setGender(user.getGender());
			u.setUsername(user.getUsername());
			u.setPassword(user.getPassword());
			u.setUserRole(user.getUserRole());
			u.setBirthDate(user.getBirthDate());
			u.setCustomerTypeId(user.getCustomerTypeId());
			u.setManagerFactoryId(user.getManagerFactoryId());
			u.setPoints(user.getPoints());
		}
		saveUsers();
		return u;
	}
	
	private void loadUsers() {
	    BufferedReader in = null;
	    try {
	        File file = new File(contextPath + "/users.txt");
	        System.out.println(file.getCanonicalPath());
	        in = new BufferedReader(new FileReader(file));
	        String line;
	        while ((line = in.readLine()) != null) {
	            line = line.trim();
	            if (line.equals("") || line.startsWith("#"))
	                continue;
	            
	            String[] parts = line.split(";");
	            String id = parts[0].trim();
	            String firstName = parts[1].trim();
	            String lastName = parts[2].trim();
	            Gender gender = Gender.valueOf(parts[3].trim());
	            String username = parts[4].trim();
	            String password = parts[5].trim();
	            UserRole userRole = UserRole.valueOf(parts[6].trim());
	            Date birthDate = dateFormat.parse(parts[7].trim());
	            String managerFactoryId = parts[8].trim();
	            Double points = Double.parseDouble(parts[9].trim());
	            
	            // Kreiranje novog korisnika i dodavanje u mapu
	            User user = new User(id, firstName, lastName, gender, username, password, userRole, birthDate, managerFactoryId, points);
	            users.put(id, user); // Koristimo username kao ključ
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (in != null) {
	            try {
	                in.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

	
	    // Metoda za spremanje korisnika u datoteku
	    private void saveUsers() {
	        BufferedWriter out = null;
	        try {
	            File file = new File(contextPath + "/users.txt");
	            System.out.println("File path: " + file.getCanonicalPath());
	            out = new BufferedWriter(new FileWriter(file, false)); // false to overwrite the file
	            for (User user : users.values()) {
	                String birthDateFormatted = dateFormat.format(user.getBirthDate());
	                String line = String.join(";",
	                        user.getId(),
	                        user.getFirstName(),
	                        user.getLastName(),
	                        user.getGender().name(),
	                        user.getUsername(),
	                        user.getPassword(),
	                        user.getUserRole().name(),
	                        birthDateFormatted,
	                        user.getManagerFactoryId(),
	                        String.valueOf(user.getPoints())
	                );
	                out.write(line);
	                out.newLine();
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (out != null) {
	                try {
	                    out.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	}

