package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import beans.Chocolate;
import beans.Chocolate.StockStatus;
import beans.Location;
import beans.Product;
import beans.User;

public class LocationDAO {
	
	private HashMap<String, Location> locations = new HashMap<String, Location>();
	private String contextPath;
	
	public LocationDAO() {
		
	}
	
	/***
	 * @param contextPath Putanja do aplikacije u Tomcatu. Moze se pristupiti samo iz servleta.
	 */
	public LocationDAO(String contextPath) {
		this.contextPath = contextPath;
		loadLocations();
	}

	/***
	 * Vraca sve proizvode.
	 * @return
	 */
	public Collection<Location> findAll() {
		return locations.values();
	}

	/***
	 *  Vraca proizvod na osnovu njegovog id-a. 
	 *  @return Proizvod sa id-em ako postoji, u suprotnom null
	 */
	public Location findLocation(String id) {
		return locations.containsKey(id) ? locations.get(id) : null;
	}
	
	public Location save(Location location) {
		Integer maxId = -1;
		for (String id : locations.keySet()) {
			int idNum =Integer.parseInt(id);
			if (idNum > maxId) {
				maxId = idNum;
			}
		}
		maxId++;
		location.setId(maxId.toString());
		locations.put(location.getId(), location);
		saveLocations();
		return location;
	}
	
	private void loadLocations() {
	    BufferedReader in = null;
	    try {
	        File file = new File(contextPath + "/locations.txt");
	        System.out.println(file.getCanonicalPath());
	        in = new BufferedReader(new FileReader(file));
	        String line;
	        while ((line = in.readLine()) != null) {
	            line = line.trim();
	            if (line.equals("") || line.indexOf('#') == 0)
	                continue;
	            String[] parts = line.split(";");
	            String id = parts[0].trim();
	            double latitude = Double.parseDouble(parts[1].trim());
	            double longitude = Double.parseDouble(parts[2].trim());
	            String streetAndNumber = parts[3].trim();
	            String city = parts[4].trim();
	            String postalCode = parts[5].trim();

	            locations.put(id, new Location(id, latitude, longitude, streetAndNumber, city, postalCode));
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (in != null) {
	            try {
	                in.close();
	            } catch (Exception e) { }
	        }
	    }
	}

	private void saveLocations() {
	    System.out.println("ULAZIM OVDE");
	    BufferedWriter out = null;
	    try {
	        File file = new File(contextPath + "/locations.txt");
	        System.out.println("Fajl putanja: " + file.getCanonicalPath());
	        out = new BufferedWriter(new FileWriter(file, false)); // false to overwrite the file
	        for (Location location : locations.values()) {
	            String line = String.join(";",
	                location.getId(),
	                String.valueOf(location.getLatitude()),
	                String.valueOf(location.getLongitude()),
	                location.getStreetAndNumber(),
	                location.getCity(),
	                location.getPostalCode()
	            );
	            out.write(line);
	            out.newLine();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        if (out != null) {
	            try {
	                out.close();
	            } catch (Exception e) { }
	        }
	    }
	}



}
