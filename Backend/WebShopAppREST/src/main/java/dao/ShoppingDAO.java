package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import beans.Shopping;
import beans.Shopping.ShoppingStatus;

public class ShoppingDAO {
    
    private HashMap<String, Shopping> shoppings = new HashMap<>();
    private String contextPath;

    public ShoppingDAO(String contextPath) {
        this.contextPath = contextPath;
        loadShoppings(); // Load shoppings from file when DAO is initialized
    }

    public ArrayList<Shopping> findAll() {
        return new ArrayList<>(shoppings.values());
    }
    
    public List<Shopping> findAllByUserId(String userId) {
        return shoppings.values().stream()
                .filter(shopping -> shopping.getCustomerId().equals(userId))
                .collect(Collectors.toList());
    }
    public List<Shopping> findAllByFactoryId(String factoryId) {
        return shoppings.values().stream()
                .filter(shopping -> shopping.getFactoryId().equals(factoryId))
                .collect(Collectors.toList());
    }

    public Shopping findShopping(String id) {
        return shoppings.get(id);
    }

    public Shopping save(Shopping shopping) {
        Integer maxId = shoppings.keySet().stream()
                .map(Integer::parseInt)
                .max(Integer::compare)
                .orElse(0);
        String nextId = String.valueOf(maxId + 1);
        shopping.setId(nextId);
        shoppings.put(nextId, shopping);
        saveShoppings();
        return shopping;
    }

    public Shopping deleteShopping(String id) {
        Shopping removedShopping = shoppings.remove(id);
        saveShoppings();
        return removedShopping;
    }

    public Shopping updateShopping(String id, Shopping updatedShopping) {
        if (shoppings.containsKey(id)) {
            updatedShopping.setId(id); // Ensure the updated shopping retains its original ID
            shoppings.put(id, updatedShopping);
            saveShoppings();
            return updatedShopping;
        }
        return null; // Shopping with given id not found
    }

    private void loadShoppings() {
        BufferedReader reader = null;
        try {
            File file = new File(contextPath + "/shoppings.txt");
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || line.startsWith("#")) {
                    continue; // Skip empty lines or comments
                }
                String[] parts = line.split(";");
                String id = parts[0].trim();
                List<String> chocolates = deserializeChocolates(parts[1].trim());
                String factoryId = parts[2].trim();
                Double price = Double.parseDouble(parts[3].trim());
                String customerName = parts[4].trim();
                ShoppingStatus shoppingStatus = ShoppingStatus.valueOf(parts[5].trim());
                String customerId = parts[6].trim();
                Shopping shopping = new Shopping(id, chocolates, factoryId, price, customerName, shoppingStatus, customerId);
                shoppings.put(id, shopping);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void saveShoppings() {
        BufferedWriter writer = null;
        try {
            File file = new File(contextPath + "/shoppings.txt");
            writer = new BufferedWriter(new FileWriter(file, false)); // false to overwrite the file
            for (Shopping shopping : shoppings.values()) {
                String line = String.format("%s;%s;%s;%s;%s;%s;%s",
                        shopping.getId(),
                        serializeChocolates(shopping.getChocolates()),
                        shopping.getFactoryId(),
                        String.valueOf(shopping.getPrice()),
                        shopping.getCustomerName(),
                        shopping.getShoppingStatus().name(),
                        shopping.getCustomerId());
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<String> deserializeChocolates(String chocolatesString) {
        return List.of(chocolatesString.split(","));
    }

    private String serializeChocolates(List<String> chocolates) {
        return String.join(",", chocolates);
    }
}
