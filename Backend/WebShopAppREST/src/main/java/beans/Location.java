package beans;

public class Location{
	private String id;
	private double latitude;
	private double longitude;
    private String streetAndNumber;
    private String city;
    private String postalCode;

	
	public Location() {
		
	}
	
	public Location(String id, double latitude, double longitude, String streetAndNumber, String city, String postalCode) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.streetAndNumber = streetAndNumber;
		this.city = city;
		this.postalCode = postalCode;
	}

	public void setId(String i) {
		id = i;
	}

	public String getId() {
		return id;
	}
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getStreetAndNumber() {
		return streetAndNumber;
	}

	public void setStreetAndNumber(String streetAndNumber) {
		this.streetAndNumber = streetAndNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	
	
}