package beans;

import java.io.Serializable;
import java.util.Date;

import javax.json.bind.annotation.JsonbDateFormat;

public class User implements Serializable {
	
	public enum UserRole {
		CUSTOMER,
		WORKER,
		MANAGER,
		ADMINISTRATOR
	}
	public enum Gender{
		MALE,
		FEMALE
	}
	
	private String id;
	private String firstName;
	private String lastName;
	private Gender gender;
	private String username;
	private String password;
	private UserRole userRole;
    @JsonbDateFormat("dd-MM-yyyy HH:mm:ss")
	private Date birthDate;
	private String customerTypeId;
	private String managerFactoryId;
	private Double points;
	
	public String getManagerFactoryId() {
		return managerFactoryId;
	}

	public void setManagerFactoryId(String managerFactoryId) {
		this.managerFactoryId = managerFactoryId;
	}

	public User() {
	}

	public User(String firstName, String lastName, Gender gender, String username, String password, UserRole userRole, Date birthDate, String managerFactoryId, Double points) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.username = username;
		this.password = password;
		this.userRole = userRole;
		this.birthDate = birthDate;
		this.managerFactoryId = managerFactoryId;
		this.points = points;
	}
	
	public User(String id, String firstName, String lastName, Gender gender, String username, String password, UserRole userRole, Date birthDate, String managerFactoryId, Double points) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.username = username;
		this.password = password;
		this.userRole = userRole;
		this.birthDate = birthDate;
		this.managerFactoryId = managerFactoryId;
		this.points = points;
	}

	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public String getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(String customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Double getPoints() {
		return points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", username=" + username
				+ ", password=" + password + "]";
	}

	private static final long serialVersionUID = 6640936480584723344L;
}
