package beans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import javax.json.bind.annotation.JsonbDateFormat;
import java.util.Date;
import java.util.List;

public class Shopping{
	
	public enum ShoppingStatus {
	    PROCESSING,
	    APPROVED,
	    DECLINED,
	    CANCELED;
	}
	
	//private static final LocalDateTime FIXED_SHOPPING_DATE = LocalDateTime.parse("2024-01-01T12:00:00");
	
	private String id;
	private List<String> chocolates;
	private String factoryId;
//	@JsonbDateFormat("yyyy-MM-dd'T'HH:mm:ss")
	//private LocalDateTime shoppingDate;
	private Double price;
	private String customerName;
	private String customerId;
	private ShoppingStatus shoppingStatus;
	
	public Shopping() {}
	
	public Shopping(String id, List<String> chocolates, String factoryId, /*LocalDateTime shoppingDate,*/ Double price,
			String customerName, ShoppingStatus shoppingStatus, String customerId) {
		super();
		this.id = id;
		this.chocolates = chocolates;
		this.factoryId = factoryId;
		//this.shoppingDate = FIXED_SHOPPING_DATE;
		this.price = price;
		this.customerName = customerName;
		this.shoppingStatus = shoppingStatus;
		this.customerId = customerId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getChocolates() {
		return chocolates;
	}

	public void setChocolates(List<String> chocolates) {
		this.chocolates = chocolates;
	}

	public String getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}
/*
	public LocalDateTime getShoppingDate() {
		return shoppingDate;
	}

	public void setShoppingDate(LocalDateTime shoppingDate) {
		this.shoppingDate = shoppingDate;
	}
*/
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public ShoppingStatus getShoppingStatus() {
		return shoppingStatus;
	}

	public void setShoppingStatus(ShoppingStatus shoppingStatus) {
		this.shoppingStatus = shoppingStatus;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}		
	
}