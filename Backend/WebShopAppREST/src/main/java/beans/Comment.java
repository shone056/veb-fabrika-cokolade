package beans;

public class Comment{
	
	private String id;
	private String buyerId;
	private String factoryId;
	private String content;
	private int rating;
	
	public Comment() {
		
	}
	
    public Comment(String id, String buyerId, String factoryId, String content, int rating) {
    	this.id = id;
    	this.buyerId = buyerId;
        this.factoryId = factoryId;
        this.content = content;
        this.rating = rating;
    }

	public String getBuyerId() {
		return buyerId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
    
    
}