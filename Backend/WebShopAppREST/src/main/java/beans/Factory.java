package beans;

import java.util.List;

public class Factory{
	
	public enum Status {
	    WORKING,
	    NOT_WORKING;
	}

	
	private String id;
	private String name;
	//radno vreme
    private String locationId;
    //private String image;
	private Status status;
	private double rating;
	private String workingTime;
	private String logo;
    
    public Factory() {
    	
    }
//    
//    public Factory(String id, String name, List<Chocolate> chocolates, Status status, Location location, String image, double rating) {
//        this.id = id;
//        this.name = name;
//        this.chocolates = chocolates;
//        this.status = status;
//        this.location = location;
//        this.image = image;
//        this.rating = rating;
//    }
    
    public Factory(String id, String name, String locationId, Status status, double rating, String workingTime, String logo) {
        this.id = id;
        this.name = name;
        //this.chocolates = chocolates;
        this.status = status;
        this.locationId = locationId;
        //this.image = image;
        this.rating = rating;
        this.workingTime = workingTime;
        this.logo = logo;
    }

	public String getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(String workingTime) {
		this.workingTime = workingTime;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
//
//	public List<Chocolate> getChocolates() {
//		return chocolates;
//	}
//
//	public void setChocolates(List<Chocolate> chocolates) {
//		this.chocolates = chocolates;
//	}
//
//	public String getImage() {
//		return image;
//	}
//
//	public void setImage(String image) {
//		this.image = image;
//	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
    
}