package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.User;
import dao.UserDAO;


@Path("/users")
public class UserService {
	@Context
	ServletContext ctx;
	
	public UserService() {
	}
	
	@PostConstruct
	// ctx polje je null u konstruktoru, mora se pozvati nakon konstruktora (@PostConstruct anotacija)
	public void init() {
		// Ovaj objekat se instancira vise puta u toku rada aplikacije
		// Inicijalizacija treba da se obavi samo jednom
		if (ctx.getAttribute("userDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("userDAO", new UserDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> getUsers() {
		System.out.println("BAR SAM TU");
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.findAll();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getById(@PathParam("id") String id) {
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.findUser(id);
	}
	
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public User newUser(User user) {
		System.out.println("TU SAMM");
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		System.out.println("OVDE SAM");
		return dao.save(user);
	}
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public User logInUser(User user) {
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.find(user.getUsername(), user.getPassword());
	}
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public User updateUser(@PathParam("id") String id, User user) {
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.updateUser(id, user);
	}
	
	@GET
	@Path("/search/managers")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> findAvailableManagers(){
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.findAvailableManagers();
	}
	
	@GET
	@Path("/search/workers")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> findAllWorkers(){
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.findAllWorkers();
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<User> searchUser(@QueryParam("firstName") String firstName,
							            @QueryParam("lastName") String lastName,
							            @QueryParam("username") String username){
		UserDAO dao = (UserDAO) ctx.getAttribute("userDAO");
		return dao.searchUser(firstName, lastName, username);
	}
	
	
		
}
