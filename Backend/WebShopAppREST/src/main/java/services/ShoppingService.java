package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Shopping;
import dao.ShoppingDAO;

@Path("/shoppings")
public class ShoppingService {
    
    @Context
    ServletContext ctx;

    public ShoppingService() {
    }

    @PostConstruct
    public void init() {
        if (ctx.getAttribute("shoppingDAO") == null) {
            String contextPath = ctx.getRealPath("");
            ctx.setAttribute("shoppingDAO", new ShoppingDAO(contextPath));
        }
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Shopping> getShoppings() {
        ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Shopping getShopping(@PathParam("id") String id) {
        ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.findShopping(id);
    }
    
    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Shopping> searchProducts(@QueryParam("customerId") String userId) {
    	System.out.println("UserId: " + userId);
    	ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.findAllByUserId(userId);
    }
    @GET
    @Path("/searchByFactory")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Shopping> searchProductsByFactory(@QueryParam("factoryId") String factoryId) {
        System.out.println("FactoryId: " + factoryId);
        ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.findAllByFactoryId(factoryId);
    }
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Shopping newShopping(Shopping shopping) {
        ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.save(shopping);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Shopping deleteShopping(@PathParam("id") String id) {
        ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.deleteShopping(id);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Shopping updateShopping(@PathParam("id") String id, Shopping shopping) {
        ShoppingDAO dao = (ShoppingDAO) ctx.getAttribute("shoppingDAO");
        return dao.updateShopping(id, shopping);
    }

}
