package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Location;
import beans.User;
import dao.LocationDAO;
import dao.UserDAO;

@Path("/locations")
public class LocationService {
	@Context
	ServletContext ctx;
	
	public LocationService() {
	}
	
	@PostConstruct
	// ctx polje je null u konstruktoru, mora se pozvati nakon konstruktora (@PostConstruct anotacija)
	public void init() {
		// Ovaj objekat se instancira vise puta u toku rada aplikacije
		// Inicijalizacija treba da se obavi samo jednom
		if (ctx.getAttribute("locationDAO") == null) {
	    	String contextPath = ctx.getRealPath("");
			ctx.setAttribute("locationDAO", new LocationDAO(contextPath));
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Location> getLocations() {
		LocationDAO dao = (LocationDAO) ctx.getAttribute("locationDAO");
		return dao.findAll();
	}

	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Location getLocation(@PathParam("id") String id) {
		LocationDAO dao = (LocationDAO) ctx.getAttribute("locationDAO");
		return dao.findLocation(id);
	}

	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Location newLocation(Location location) {
		LocationDAO dao = (LocationDAO) ctx.getAttribute("locationDAO");
		return dao.save(location);
	}
	
		
}
